'use strict'

const randomNumber = (min, max) => {
    return Math.floor(Math.random() * max) + min
}

module.exports = randomNumber