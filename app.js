'use strict'

const express = require('express'),
      bodyParser = require('body-parser'),
      cors = require('cors');
const db = require('./app/services/database')

var app = express()

app.use(cors())

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(express.static(__dirname + '/build'))

app.use('/api', require('./app/routes/api')())

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname + '/build/index.html'))
})

const port = process.env.PORT || 7777

app.listen(port, () => {
    db.authenticate()
      .then(() => {
          db.sync()
            .then(() => {
                console.log('Listening on ', + port)
            })
      })
      .catch(() => {
          throw new Error()
      })
})