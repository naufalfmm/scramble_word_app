'use strict'

const syllable = require('syllable')

const factorial = (number) => {
    var res = 1
    for (var i = 2; i < number + 1; i++) {
        res *= i
    }

    return res
}

const extractWord = (word) => {
    var wordProperties = {}

    for (var i = 0; i < word.length; i++) {
        if (word[i] in wordProperties) {
            wordProperties[word[i]]++
        } else {
            wordProperties[word[i]] = 1
        }
    }

    return wordProperties
}

const consecutiveFactor = (wordProperties) => {
    var result = 1

    for (var letter in wordProperties) {
        if (wordProperties[letter] > 1) {
            result *= factorial(wordProperties[letter])
        }
    }

    return result
}

const permutationScore = (word) => {
    const wordPermutation = factorial(word.length)
    const wordProperties = extractWord(word)
    const wordConsecutivePermutation = wordPermutation / consecutiveFactor(wordProperties)

    return wordConsecutivePermutation / wordPermutation
}

const syllablesScore = (word) => {
    return 1 / syllable(word)
}

const wordScore = (word) => {
    return word.length + permutationScore(word) + syllablesScore(word)
}

const wordDifficulty = (word) => {
    const score = wordScore(word)

    if (score < 6.5) {
        return "easy"
    } else if (score < 9.5) {
        return "medium"
    } else {
        return "hard"
    }
}

module.exports = wordDifficulty