'use strict'

const Playingword = require('../models/playingword'),
      Word = require('../models/word'),
      Play = require('../models/play');
const wordController = require('./wordController')
const shuffleWord = require('../lib/shuffle')

var playingwordController = {}

playingwordController.postPlayingwordOff = (word, play_id, t) => {
    console.log('postPlayingwordOff')
    return new Promise((resolve, reject) => {
        Playingword
            .build({ playingword_word_id: word.dataValues.word_id, playingword_play_id: play_id })
            .save({ transaction: t })
            .then(res => {
                resolve({ id: res.playingword_id, random: shuffleWord(word.dataValues.word_text), explanation: word.dataValues.word_explanation })
            })
            .catch(err => {
                console.log('postPlayingwordOff reject', err)
                reject({ code: 500, status: false, message: "Internal Server Error" })
            })
    })
}

playingwordController.createPlayingwordsOff = (play_id, t) => {
    console.log('createPlayingwordsOff')
    return new Promise((resolve, reject) => {
        var promises = [wordController.getRandomWordsOff(2, "easy"), wordController.getRandomWordsOff(2, "medium"), wordController.getRandomWordsOff(1, "hard")]

        Promise.all(promises)
            .then(result => {
                // console.log('createPlayingwordsOff', 2, typeof result)
                var promisesWord = []
                for (var i=0; i<result.length; i++) {
                    // var resultTh = result[i]
                    for (var j=0; j<result[i].length; j++) {
                        // console.log('createPlayingwordsOff', typeof result[i])
                        promisesWord.push(playingwordController.postPlayingwordOff(result[i][j], play_id, t))
                    }
                }
                
                Promise.all(promisesWord)
                    .then(res => {
                        // console.log('createPlayingwordsOff', 3, res)
                        resolve(res)
                    })
                    .catch(err => {
                        console.log('createPlayingwordsOff reject 1')
                        reject(err)
                    })
            })
            .catch(error => {
                console.log('createPlayingwordsOff reject 2')
                reject(error)
            })
    })
}

/*
link: api/check/:playingwordid
json: {
    answer:
}
*/
playingwordController.checkAnswer = (req, res) => {
    const playingwordId = req.params.playingwordid
    const answer = req.body.answer

    if (playingwordId === undefined || answer === undefined) {
        return res.status(400).json({ status: false, message: "Bad Request" })
    }

    Playingword.findById(playingwordId, { include: [ { model: Word }, { model: Play } ] })
        .then(playingword => {
            // console.log(playingword.play.play_end, playingword.play.play_end === null)
            if (!playingword.playingword_answer && playingword.play.play_end === null) {
                const trueAnswer = playingword.word.word_text
                if (answer.length != trueAnswer.length) {
                    return res.status(400).json({ status: false, message: "Answer Not Complete" })
                }

                var check = []
                // var score = 0
                var right = true

                for (var i = 0; i < answer.length; i++) {
                    if (answer[i] != trueAnswer[i]) {
                        // score -= 5
                        right = false
                        check.push(0)
                    } else {
                        // score += 10
                        check.push(1)
                    }
                }

                playingword.playingword_right = right
                playingword.playingword_answer = true
                playingword.save()
                    .then(() => {
                        return res.status(200).json({ status: true, checkResult: check, trueAnswer: trueAnswer, isRight: right })
                    })
            } else {
                if (playingword.play.play_end !== null) {
                    return res.status(403).json({ status: false, message: "Game Has Finished" })
                } else {
                    return res.status(403).json({ status: false, message: "Word Has Been Answered" })
                }
            }
        })
        .catch(err => {
            return res.status(500).json({ status: true, message: "Internal Server Error" })
        })
}

module.exports = playingwordController