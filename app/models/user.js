'use strict'

const Sequelize = require('sequelize')
const db = require('../services/database')

const modelDefinition = {
    user_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    user_email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        validate: {
            isEmail: true
        }
    }
}

const userModel = db.define("user", modelDefinition)

module.exports = userModel