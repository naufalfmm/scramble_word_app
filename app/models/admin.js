'use strict'

const Sequelize = require('sequelize'),
      bcrypt = require('bcrypt');
const db = require('../services/database');

const modelDefinition = {
    admin_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    admin_username: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    admin_password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    admin_email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        validate: {
            isEmail: true
        }
    }
}

var adminModel = db.define("admin", modelDefinition)

adminModel.addHook('afterSync', 'addAdmin', async (opt) => {
    // console.log("afterSync")
    adminModel.count().then(c => {
        if (c === 0) {
            var newAdmin = {
                admin_username: 'admin',
                admin_password: 'admin',
                admin_email: "admin@admin.com"
            }
            adminModel.create(newAdmin)
        }
    }).catch(() => {
        throw new Error()
    })
})

adminModel.addHook('beforeValidate', 'hashPassword', (admin) => {
    // console.log("beforeValidate", admin.changed('admin_password'))
    if (admin.changed('admin_password')) {
        return bcrypt.hash(admin.admin_password, 10).then(function (pass) {
            admin.admin_password = pass
        }).catch(function (e) {
            throw new Error()
        })
    }
})

function hashPassword(admin) {
    if (admin.changed('admin_password')) {
        return bcrypt.hash(admin.admin_password, 10).then(function (password) {
            admin.admin_password = password;
        });
    }
}


module.exports = adminModel