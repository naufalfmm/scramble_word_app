'use strict'

const Sequelize = require('sequelize')
const db = require('../services/database')
const Word = require('./word')
const Play = require('./play')

const modelDefinition = {
    playingword_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    playingword_right: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    // playingword_score: {
    //     type: Sequelize.INTEGER,
    //     allowNull: false,
    //     defaultValue: 0
    // },
    playingword_answer: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    }
}

const playingwordModel = db.define("playingword", modelDefinition)

Word.hasMany(playingwordModel, { foreignKey: { name: 'playingword_word_id', allowNull: false } })
playingwordModel.belongsTo(Word, { foreignKey: { name: 'playingword_word_id', allowNull: false } })

Play.hasMany(playingwordModel, { foreignKey: { name: 'playingword_play_id', allowNull: false } })
playingwordModel.belongsTo(Play, { foreignKey: { name: 'playingword_play_id', allowNull: false } })

module.exports = playingwordModel