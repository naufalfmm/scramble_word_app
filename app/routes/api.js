'use strict'

var router = require('express').Router()
const PlayController = require('../controllers/playController'),
      PlayingwordController = require('../controllers/playingwordController'),
    //   WordController = require('../controllers/wordController'),
      UserController = require('../controllers/userController'),
      AdminController = require('../controllers/adminController');

const APIRoutes = () => {
    router.post('/admin/login', AdminController.login)

    router.post('/user/login', UserController.login)

    router.post('/play/create', PlayController.createPlay)
    router.post('/play/finish', PlayController.finishPlay)
    router.get('/score/:userId', PlayController.getAllScore)

    router.post('/check/:playingwordid(\\d+)/', PlayingwordController.checkAnswer)

    return router
}

module.exports = APIRoutes