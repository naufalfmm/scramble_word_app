'use strict'

const jwt = require('jsonwebtoken'),
      bcrypt = require('bcrypt');
const config = require('../config/index')
const Admin = require('../models/admin')

var adminController = {}

/*
link: api/admin/login
body: {
    username:,
    password:
}
*/
adminController.login = (req, res) => {
    if (!req.body.username || !req.body.password) {
        return res.status(400).json({ status: false, message: "Bad Request" })
    }

    const username = req.body.username
    const password = req.body.password

    Admin.sync()
        .then(() => {
            Admin.findOne({ where: { admin_username: username } })
                .then(admin => {
                    bcrypt.compare(password, admin.admin_password)
                        .then(isMatch => {
                            if (!isMatch) {
                                return res.status(401).json({ status: false, message: "Unauthorized" })
                            } else {
                                var token = jwt.sign({ username: username, status: statusAdmin }, config.hmac_admin, { expiresIn: '30000m' })

                                return res.status(200).json({ status: true, token: token, message: "Happy Login!" })
                            }
                        })
                        .catch(err => {
                            return res.status(500).json({ status: false, message: "Internal Server Error" })
                        })
                })
                .catch(err => {
                    return res.status(500).json({ status: false, message: "Internal Server Error" })
                })
        })
        .catch(() => {
            return res.status(500).json({ status: false, message: "Internal Server Error" })
        })
}

module.exports = adminController