'use strict'

const randomNumber = require('./random')

const shuffleWord = (word) => {
    var wordShuff = word.split("")
    for (var i = 0; i <= word.length - 2; i++) {
        var j = randomNumber(i, word.length - 1)
        var b = wordShuff[i]
        
        wordShuff[i] = wordShuff[j]
        wordShuff[j] = b
    }

    return wordShuff.join("")
}

module.exports = shuffleWord