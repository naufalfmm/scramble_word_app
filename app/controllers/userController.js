'use strict'

const jwt = require('jsonwebtoken')
const config = require('../config/index')
const User = require('../models/user')

var userController = {}

/*
link: api/user/login
body: {
    name:,
    email:
}
*/
userController.login = (req, res) => {
    if (!req.body.name || !req.body.email) {
        return res.status(400).json({ status: false, message: "Bad Request" })
    }

    const name = req.body.name
    const email = req.body.email

    User.findOrCreate({ where: { user_email: email } , defaults: { user_name: name }})
        .spread((user, isCreated) => {
            return res.status(200).json({ status: true, user_id: user.user_id, message: "Happy Login!" })
        })
        .catch(err => {
            return res.status(500).json({ status: false, message: "Internal Server Error" })
        })
}

module.exports = userController