insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('abaca','The Manila-hemp plant (Musa textilis); also, its fiber. See Manila hemp under Manila.','easy','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('abactinal','Pertaining to the surface or end opposite to the mouth in a radiate animal','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('atmosphere','The pressure or weight of the air at the sea level, on a unit of surface, or about 14.7 Ibs. to the sq. inch.','hard','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('apparel','A small ornamental piece of embroidery worn on albs and some other ecclesiastical vestments.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('apozem','A decoction or infusion.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('inspirit','To infuse new life or spirit into','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('instar','To stud as with stars.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('instruct','Arranged','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('insulated','Situated at so great a distance as to be beyond the effect of gravitation','hard','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('intact','Untouched, especially by anything that harms, defiles, or the like; uninjured; undefiled; left complete or entire.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('quack','To utter a sound like the cry of a duck.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('quadra','The plinth, or lowest member, of any pedestal, podium, water table, or the like.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('quagmire','Soft, wet, miry land, which shakes or yields under the feet.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('quality','The condition of being of such and such a sort as distinguished from others; nature or character relatively considered, as of goods; character; sort; rank.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('quantum','Quantity','hard','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('xanthamide','An amido derivative of xanthic acid obtained as a white crystalline substance, C2H5O.CS.NH2; -- called also xanthogen amide.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('xanthous','Yellow; specifically (Ethnol.), of or pertaining to those races of man which have yellowish, red, auburn, or brown hair.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('xeronic','Pertaining to, or designating, an acid, C8H12O4, related to fumaric acid, and obtained from citraconic acid as an oily substance having a bittersweet taste; -- so called from its tendency to form its anhydride.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('xyletic','Pertaining to, or designating, a complex acid related to mesitylenic acid, obtained as a white crystalline substance by the action of sodium and carbon dioxide on crude xylenol.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('xylogen','Lignin.','medium','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('fan','An instrument used for producing artificial currents of air, by the wafting or revolving motion of a broad surface','easy','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('car','A small vehicle moved on wheels; usually, one having but two wheels and drawn by one horse; a cart.','easy','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('easy','At ease; free from pain, trouble, or constraint','easy','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('game','Crooked; lame; as, a game leg.','easy','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('gypsy','A cunning or crafty person','easy','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('can','A drinking cup','easy','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('subscribe','To write underneath, as ones name; to sign (ones name) to a document.','hard','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('wildebeest','The gnu.','hard','27/08/2018 21:53:01','28/08/2018 21:53:01');
insert into words("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt") values('nunciature','The office of a nuncio.','hard','27/08/2018 21:53:01','28/08/2018 21:53:01');

insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('abaca', 'The Manila-hemp plant (Musa textilis); also, its fiber. See Manila hemp under Manila.', 'easy', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('abactinal', 'Pertaining to the surface or end opposite to the mouth in a radiate animal', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('atmosphere', 'The pressure or weight of the air at the sea level, on a unit of surface, or about 14.7 Ibs. to the sq. inch.', 'hard', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('apparel', 'A small ornamental piece of embroidery worn on albs and some other ecclesiastical vestments.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('apozem', 'A decoction or infusion.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('inspirit', 'To infuse new life or spirit into', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('instar', 'To stud as with stars.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('instruct', 'Arranged', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('insulated', 'Situated at so great a distance as to be beyond the effect of gravitation', 'hard', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('intact', 'Untouched, especially by anything that harms, defiles, or the like; uninjured; undefiled; left complete or entire.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('quack', 'To utter a sound like the cry of a duck.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('quadra', 'The plinth, or lowest member, of any pedestal, podium, water table, or the like.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('quagmire', 'Soft, wet, miry land, which shakes or yields under the feet.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('quality', 'The condition of being of such and such a sort as distinguished from others; nature or character relatively considered, as of goods; character; sort; rank.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('quantum', 'Quantity', 'hard', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('xanthamide', 'An amido derivative of xanthic acid obtained as a white crystalline substance, C2H5O.CS.NH2; -- called also xanthogen amide.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('xanthous', 'Yellow; specifically (Ethnol.), of or pertaining to those races of man which have yellowish, red, auburn, or brown hair.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('xeronic', 'Pertaining to, or designating, an acid, C8H12O4, related to fumaric acid, and obtained from citraconic acid as an oily substance having a bittersweet taste; -- so called from its tendency to form its anhydride.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('xyletic', 'Pertaining to, or designating, a complex acid related to mesitylenic acid, obtained as a white crystalline substance by the action of sodium and carbon dioxide on crude xylenol.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('xylogen', 'Lignin.', 'medium', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('fan', 'An instrument used for producing artificial currents of air, by the wafting or revolving motion of a broad surface', 'easy', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('car', 'A small vehicle moved on wheels; usually, one having but two wheels and drawn by one horse; a cart.', 'easy', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('easy', 'At ease; free from pain, trouble, or constraint', 'easy', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('game', 'Crooked; lame; as, a game leg.', 'easy', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('gypsy', 'A cunning or crafty person', 'easy', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('can', 'A drinking cup', 'easy', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('subscribe', 'To write underneath, as ones name; to sign (ones name) to a document.', 'hard', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('wildebeest', 'The gnu.', 'hard', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
insert into words
    ("word_text", "word_explanation", "word_difficulty", "createdAt", "updatedAt")
values('nunciature', 'The office of a nuncio.', 'hard', '2019-01-17 21:53:01', '2019-01-19 21:53:01');
