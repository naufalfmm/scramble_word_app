'use strict'

const Word = require('../models/word')
const randomNumber = require('../lib/random')

var wordController = {}

wordController.getRandomWordsOff = (count, level) => {
    return new Promise((resolve, reject) => {
        var ret = []
        var numberRand = 0

        Word.findAndCountAll({ where: { word_difficulty: level } })
            .then(res => {
                // console.log('getRandomWordsOff', res.rows.length, res.rows[0])
                if (res.count < count) {
                    reject({ code: 404, status: false, message: "No Word Found" })
                } else {
                    for (var i = 0; i < count; i++) {
                        numberRand = randomNumber(0, res.count)
                        while (res.rows[numberRand] === undefined) {
                            numberRand = randomNumber(0, res.count)
                        }
                        // console.log('getRandomWordsOff', numberRand)

                        // delete res.rows[numberRand]

                        ret.push(res.rows[numberRand])
                    }

                    resolve(ret)
                }
            })
            .catch(() => {
                reject({ code: 500, status: false, message: "Internal Server Error" })
            })
    })
    // console.log('getRandomWordsOff')
    // return new Promise((resolve, reject) => {
    //     var ret = []
    //     var numberRand = 0

    //     Word.findAndCountAll({ where: { word_difficulty: level } })
    //         .then(res => {
    //             console.log('getRandomWordsOff', res.count, count, level)
    //             if (res.count < count) {
    //                 console.log('getRandomWordsOff reject', res.count, count, level)
    //                 reject({ code: 404, status: false, message: "No Word Found" })
    //             }
    //             for (var i = 0; i < count; i++) {
    //                 numberRand = randomNumber(0, res.count)
    //                 while (res.rows[numberRand] === undefined) {
    //                     numberRand = randomNumber(0, res.count)
    //                 }

    //                 ret.push(res.rows[numberRand])
    //             }

    //             resolve(ret)
    //         })
    //         .catch(() => {
    //             reject({ code: 500, status: false, message: "Internal Server Error" })
    //         })
    // })
}

module.exports = wordController