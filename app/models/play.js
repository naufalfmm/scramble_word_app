'use strict'

const Sequelize = require('sequelize')
const db = require('../services/database')
const User = require('./user')

const modelDefinition = {
    play_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    play_end: {
        type: Sequelize.DATE,
        allowNull: true
    }
}

const modelOptions = {
    createdAt: 'play_start'
}

const playModel = db.define("play", modelDefinition, modelOptions)

User.hasMany(playModel, { foreignKey: { name: 'play_user_id', allowNull: false } })
playModel.belongsTo(User, { foreignKey: { name: 'play_user_id', allowNull: false } })

module.exports = playModel