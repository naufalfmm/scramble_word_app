'use strict'

const Sequelize = require('sequelize')
const db = require('../services/database')

const modelDefinition = {
    word_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    word_text: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        set(word) {
            this.setDataValue('word_text', word.toLowerCase())
        }
    },
    word_explanation: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: ''
    },
    word_difficulty: {
        type: Sequelize.ENUM('easy', 'medium', 'hard'),
        allowNull: false
    }
}

const wordModel = db.define("word", modelDefinition)

module.exports = wordModel