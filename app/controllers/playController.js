'use strict'

const Sequelize = require('sequelize')
const db = require('../services/database')
const Play = require('../models/play')
const Playingword = require('../models/playingword')
const playingwordController = require('./playingwordController')

var playController = {}

/*
link: /api/play/create
*/
playController.createPlay = (req, res) => {
    const userId = req.body.user_id

    if (userId === undefined) {
        return res.status(400).json({ status: false, message: "Bad Request" })
    }
    console.log('createPlay');
    
    db.transaction().then((t) => {
        Play.build({ play_user_id: userId })
            .save({ transaction: t })
            .then(play => {
                playingwordController.createPlayingwordsOff(play.play_id, t)
                    .then(playData => {
                        t.commit()
                        return res.status(200).json({ status: true, play_id: play.play_id, play: playData })
                    })
                    .catch(err => {
                        console.log("createPlay err 1", err)
                        t.rollback()
                        return res.status(err.code).json({ status: err.status, message: err.message })
                    })
            })
            .catch(err => {
                console.log("createPlay err 2", err)
                t.rollback()
                return res.status(500).json({ status: false, message: "Internal Server Error" })
            })
    })
    // Play.build({ play_user_id: userId })
    //     .save()
    //     .then(result => {
    //         playingwordController.createPlayingwordsOff(result.play_id)
    //             .then(playData => {
    //                 return res.status(200).json({ status: true, play: playData })
    //             })
    //             .catch(err => {
    //                 return res.status(err.code).json({ status: err.status, message: err.message })
    //             })
    //     })
    //     .catch(err => {
    //         console.log(err)
    //         return res.status(500).json({ status: false, message: "Internal Server Error" })
    //     })
}

/*
link: api/play/finish
*/
playController.finishPlay = (req, res) => {
    const userId = req.body.user_id
    const playId = req.body.play_id

    if (userId === undefined || playId === undefined) {
        return res.status(400).json({ status: false, message: "Bad Request" })
    }

    Play.update({ play_end: Sequelize.fn('NOW') }, { where: { play_id: playId, play_user_id: userId } })
        .then(countUpd => {
            if (countUpd[0] <= 0) {
                return res.status(400).json({ status: false, message: "Not Found" })
            } else {
                return res.status(200).json({ status: true, message: "Success" })
            }
        })
        .catch(err => {
            console.log(err)
            return res.status(500).json({ status: false, message: "Internal Server Error" })
        })
}

/*
linkL api/score/:userId
*/
playController.getAllScore = (req, res) => {
    const userId = req.params.userId

    if (userId === undefined) {
        return res.status(400).json({ status: false, message: "Bad Request" })
    }

    Play.findAll({ where: { play_user_id: userId }, include: [{ model: Playingword }] })
        .then(playData => {
            return res.status(200).json({status: true, data: playData})
        })
        .catch(err => {
            return res.status(500).json({status: false, message: "Internal Server Error"})
        })
}

module.exports = playController